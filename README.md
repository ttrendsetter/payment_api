# RISHAT API

## Preprocessing

1. Change file default_config.yaml - set your secret and public stripe keys
```YAML
  secret_key: "YOUR_STRIPE_SK"
  public_key: "YOUR_STRIPE_SK"
```

2. Install requirements

3. Execute next commands in the console:

    python manage.py makemigrations
  
    python manage.py migrate
  
    python manage.py runserver

## API VIEW

### GET /payment/items

Response text/html - page with list of the created items
 
### GET /payment/newitem
  
Response text/html - page for creation items what we want to buy
  
### GET /payment/item/{item_id}

Response text/html - page for buying a choosen item

### GET /payment/buy/{item_id}

Response text/html - returns a string - id of new stripe session
